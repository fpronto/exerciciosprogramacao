#include <stdio.h>


/*
 *   O objetivo do programa é pedir dois números inteiros ao utilizador e guardá-los nas variáveis a e
 *   b. A seguir deve somar esses valores e guardar o resultado na variável total. Termine a 
 *   implementação da função main() sem nunca se referir explicitamente (i.e., pelo seu nome) às 
 *   variáveis a, b ou total.
 */
int main()
{
  int a, b, total, *p = &a, *q = &b, *r = &total;
  printf("Insira a: ");
  scanf("%d", p);
  printf("Insira b: ");
  scanf("%d", q);

  *r = *p + *q;

  printf("a= %d \t b= %d \t total= %d\n", a, b, total);
  return 0;
}