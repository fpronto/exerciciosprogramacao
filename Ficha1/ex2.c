#include <stdio.h>
#include <stdlib.h>

/*
 *   Escreva uma função em C que receba informação sobre 3 variáveis reais do tipo float e que 
 *   efetue uma rotação entre elas, i.e., a segunda variável deve ficar com o valor da primeira, a 
 *   terceira deve ficar com o valor da segunda e a primeira deve ficar com o valor da terceira. Ao 
 *   implementar o código defina corretamente os parâmetros da função, de forma a garantir que esta 
 *   realiza a tarefa pretendida
 */

void rotacao (float *a, float *b, float *c) {
  float d = *a;
  *a= *c;
  *c= *b;
  *b= d;

  return;
}

int main()
{
  float *a, *b, *c;
  
  a= malloc(sizeof(float));
  b= malloc(sizeof(float));
  c= malloc(sizeof(float));

  *a = 1.1;
  *b = 2.1;
  *c = 3.1;


  printf("a= %f \t b= %f \t c= %f\n", *a, *b, *c);

  rotacao(a, b, c);

  printf("a= %f \t b= %f \t c= %f\n", *a, *b, *c);
  return 0;
}