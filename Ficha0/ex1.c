#include <stdio.h>
#define N 5

/*
 *   Desenvolva uma função que encontre o maior elemento armazenado num vetor de inteiros. A 
 *   função recebe como argumentos o nome e a dimensão do vetor. Devolve como resultado o maior 
 *   valor existente no vetor.
 */

int maiorValor (int arr[], int dim) {
  int maior = arr[0];
  for(int i = 1; i < dim; i++) {
    if(arr[i]> maior) {
      maior = arr[i];
    }
  }
}

int main()
{
  int arr[N] = {1,2,3,4,5};

  printf("O maior valor: %d\n", maiorValor(arr, N));
  return 0;
}